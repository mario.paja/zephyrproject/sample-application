# An application for a sample Zephyr module

  This is a manifest and a sample application repository used for
  describing how Zephyr module works. 

##  Initiale Sample Workspace

  ```
  mkdir sample-workspace
  cd sample-workspace
  west init -m https://gitlab.com/mario.paja/zephyrproject/sample_application
  west update
  ```
